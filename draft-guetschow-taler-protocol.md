---
v: 3

title: "The GNU Taler Protocol"
docname: draft-guetschow-taler-protocol
category: info

ipr: trust200902
workgroup: independent
stream: independent
keyword:
  - taler
  - cryptography
  - ecash
  - payments

#venue:
#  repo: https://git.gnunet.org/lsd0009.git/
#  latest: https://lsd.gnunet.org/lsd0009/

author:
 -
    name: Mikolai Gütschow
    org: TUD Dresden University of Technology
    abbrev: TU Dresden
    street: Helmholtzstr. 10
    city: Dresden
    code: D-01069
    country: Germany
    email: mikolai.guetschow@tu-dresden.de

normative:
  RFC20:
  RFC2104:
  RFC5869:
  RFC6234:
  HKDF: DOI.10.1007/978-3-642-14623-7_34
  SHS: DOI.10.6028/NIST.FIPS.180-4

informative:


--- abstract

\[ TBW \]

--- middle

# Introduction

\[ TBW \]

Beware that this document is still work-in-progress and may contain errors.
Use at your own risk!

# Notation

- `"abc"` denotes the literal string `abc` encoded as ASCII [RFC20]
- `a | b` denotes the concatenation of a with b
- `padZero(12, a)` denotes the byte string a, zero-padded to the length of 12 bytes
- `bits(x)` (`bytes(x)`) denotes the minimal number of bits (bytes) necessary to represent the multiple precision integer x
- `bigEndian(16, x)` denotes the 16 least significant bits of the integer x encoded in network byte order (big-endian)
- `bigEndianAmount(amount)` is formed from a fixed-point representation of `amount`
  as `bigEndian(64, amount.value) | bigEndian(32, amount.fraction) | padZero(12, amount.currency)`,
  where `amount.value` is the non-negative integer part of the base currency,
  `amount.fraction` is given in unites of one hundred millionth (1e-8) of the base currency,
  and `amount.currency` are the 3-11 ASCII characters used as currency code by the exchange.
- `random(256)` denotes a randomly generated sequence of 256 bits
- `a * b (mod N)` denotes the multiplication, `a ** b (mod N)` the exponentiation of a and b, modulo N

# Cryptographic Primitives

## Cryptographic Hash Functions

### SHA-256 {#sha256}

~~~
SHA-256(msg) -> hash

Input:
    msg     input message of length L < 2^61 octets

Output:
    hash    message digest of fixed length HashLen = 32 octets
~~~

`hash` is the output of SHA-256 as per Sections 4.1, 5.1, 6.1, and 6.2 of [RFC6234].

### SHA-512 {#sha512}

~~~
SHA-512(msg) -> hash

Input:
    msg     input message of length L < 2^125 octets

Output:
    hash    message digest of fixed length HashLen = 64 octets
~~~

`hash` is the output of SHA-512 as per Sections 4.2, 5.2, 6.3, and 6.4 of [RFC6234].

### SHA-512-256 (truncated SHA-512) {#sha512-trunc}

~~~
SHA-512(msg) -> hash

Input:
    msg     input message of length L < 2^125 octets

Output:
    hash    message digest of fixed length HashLen = 32 octets
~~~

The output `hash` corresponds to the first 32 octets of the output of SHA-512 defined in {{sha512}}:

~~~
temp = SHA-512(msg)
hash = temp[0:31]
~~~

Note that this operation differs from SHA-512/256 as defined in [SHS] in the initial hash value.


## Message Authentication Codes

### HMAC {#hmac}

~~~
HMAC-Hash(key, text) -> out

Option:
    Hash    cryptographic hash function with output length HashLen

Input:
    key     secret key of length at least HashLen
    text    input data of arbitary length

Output:
    out     output of length HashLen
~~~

`out` is calculated as defined in [RFC2104].


## Key Derivation Functions

### HKDF {#hkdf}

The Hashed Key Derivation Function (HKDF) used in Taler is an instantiation of [RFC5869]
with two different hash functions for the Extract and Expand step as suggested in [HKDF]:
`HKDF-Extract` uses `HMAC-SHA512`, while `HKDF-Expand` uses `HMAC-SHA256` (cf. {{hmac}}).

~~~
HKDF(salt, IKM, info, L) -> OKM

Inputs:
    salt    optional salt value (a non-secret random value);
              if not provided, it is set to a string of 64 zeros.
    IKM     input keying material
    info    optional context and application specific information
              (can be a zero-length string)
    L       length of output keying material in octets
              (<= 255*32 = 8160)

Output:
    OKM      output keying material (of L octets)
~~~

The output OKM is calculated as follows:

~~~
PRK = HKDF-Extract(salt, IKM) with Hash = SHA-512 (HashLen = 64)
OKM = HKDF-Expand(PRK, info, L) with Hash = SHA-256 (HashLen = 32)
~~~

### HKDF-Mod

Based on the HKDF defined in {{hkdf}}, this function returns an OKM that is smaller than a given multiple precision integer N.

~~~
HKDF-Mod(N, salt, IKM, info) -> OKM

Inputs:
    N        multiple precision integer
    salt     optional salt value (a non-secret random value);
              if not provided, it is set to a string of 64 zeros.
    IKM      input keying material
    info     optional context and application specific information
              (can be a zero-length string)

Output:
    OKM      output keying material (smaller than N)
~~~

The final output `OKM` is determined deterministically based on a counter initialized at zero.

~~~
counter = 0
do until OKM < N:
    x = HKDF(salt, IKM, info | bigEndian(16, counter), bytes(N))
    OKM = bigEndian(bits(N), x)
    counter += 1
~~~

## Non-Blind Signatures

### Ed25519

## Blind Signatures

### RSA-FDH {#rsa-fdh}

#### Supporting Functions

~~~
RSA-FDH(msg, pubkey) -> fdh

Inputs:
    msg     message
    pubkey  RSA public key consisting of modulus N and public exponent e

Output:
    fdh     full-domain hash of msg over pubkey.N
~~~

`fdh` is calculated based on HKDF-Mod from {{hkdf-mod}} as follows:

~~~
info = "RSA-FDA FTpsW!"
salt = bigEndian(16, bytes(pubkey.N)) | bigEndian(16, bytes(pubkey.e))
     | pubkey.N | pubkey.e
fdh = HKDF-Mod(pubkey.N, salt, msg, info)
~~~

The resulting `fdh` can be used to test against a malicious RSA pubkey
by verifying that the greatest common denominator (gcd) of `fdh` and `pubkey.N` is 1.

~~~
RSA-FDH-Derive(bks, pubkey) -> out

Inputs:
    bks     blinding key secret of length L = 32 octets
    pubkey  RSA public key consisting of modulus N and public exponent e

Output:
    out     full-domain hash of bks over pubkey.N
~~~

`out` is calculated based on HKDF-Mod from {{hkdf-mod}} as follows:

~~~
info = "Blinding KDF"
salt = "Blinding KDF extractor HMAC key"
fdh = HKDF-Mod(pubkey.N, salt, bks, info)
~~~

#### Blinding

~~~
RSA-FDH-Blind(msg, bks, pubkey) -> out

Inputs:
    msg     message
    bks     blinding key secret of length L = 32 octets
    pubkey  RSA public key consisting of modulus N and public exponent e

Output:
    out     message blinded for pubkey
~~~

`out` is calculated based on RSA-FDH from {{rsa-fdh}} as follows:

~~~
data = RSA-FDH(msg, pubkey)
r = RSA-FDH-Derive(bks, pubkey)
r_e = r ** pubkey.e (mod pubkey.N)
out = r_e * data (mod pubkey.N)
~~~

#### Signing

~~~
RSA-FDH-Sign(data, privkey) -> sig

Inputs:
    data    data to be signed, an integer smaller than privkey.N
    privkey RSA private key consisting of modulus N and private exponent d

Output:
    sig     signature on data by privkey
~~~

`sig` is calculated as follows:

~~~
sig = data ** privkey.d (mod privkey.N)
~~~

#### Unblinding

~~~
RSA-FDH-Unblind(sig, bks, pubkey) -> out

Inputs:
    sig     blind signature
    bks     blinding key secret of length L = 32 octets
    pubkey  RSA public key consisting of modulus N and public exponent e

Output:
    out     unblinded signature
~~~

`out` is calculated as follows:

~~~
r = RSA-FDH-Derive(bks, pubkey)
r_inv = inverse of r (mod pubkey.N)
out = sig * r_inv (mod pubkey.N)
~~~

#### Verifying

~~~
RSA-FDH-Verify(msg, sig, pubkey) -> out

Inputs:
    msg     message
    sig     signature of pubkey over msg
    pubkey  RSA public key consisting of modulus N and public exponent e

Output:
    out     true, if sig is a valid signature
~~~

`out` is calculated based on RSA-FDH from {{rsa-fdh}} as follows:

~~~
data = RSA-FDH(msg, pubkey)
exp = sig ** pubkey.e (mod pubkey.N)
out = (data == exp)
~~~

### Clause-Schnorr

# The Taler Crypto Protocol

// todo: explain persist, check

## Withdrawal

The wallet creates `n > 0` coins and requests `n` signatures from the exchange,
attributing value to the coins according to `n` chosen denominations.
The total value and withdrawal fee (defined by the Exchange per denomination)
must be smaller or equal to the amount stored in the single reserve used for withdrawal.
The symbol `*` in front of a certain part means that it is repeated `n` times for the `n` coins,
where `?*` denotes the index number `0 <= ?* < n`.

~~~
            Wallet                                  Exchange
knows *denom.pub                        knows *denom.priv
               |                                        |
reserve = EdDSA-Keygen()                                |
persist (reserve, value)                                |
               |                                        |
               |----------- (bank transfer) ----------->|
               | (subject: reserve.pub, amount: value)  |
               |                                        |
               |                        persist (reserve.pub, value)
               |                                        |
master_secret = random(256)                             |
persist master_secret                                   |
*(coin, blind_secret) = GenerateCoin(master_secret, ?*) |
*blind_coin = RSA-FDH-Blind(SHA-512(coin.pub), blind_secret, denom.pub)
sig = EdDSA-Sign(reserve.priv, msg)                     |
               |                                        |
               |--- /reserves/{reserve.pub}/withdraw -->|
               |(*SHA-512(denom.pub), *blind_coin, sig) |
               |                                        |
               |                        check *denom.pub known and not withdrawal-expired
               |                        check EdDSA-Verify(reserve.pub, msg, sig)
               |                        check reserve KYC status ok or not needed
               |                        check reserve.balance >= sum(*denom.valueAndFee)
               |                        reserve.balance -= sum(*denom.valueAndFee)
               |                        *blind_sig = RSA-FDH-Sign(b, denom.priv)
               |                        persist withdrawal
               |                                        |
               |<------------ *blind_sig ---------------|
               |                                        |
*coin_sig = RSA-FDH-Unblind(blind_sig, blind_secret, denom.pub)
check *RSA-FDH-Verify(SHA-512(coin.pub), coin_sig, denom.pub)
persist *(coin, blind_secret, coin_sig)
~~~

where `msg` is formed as follows:

~~~
commit = reserve.pub
       | bigEndianAmount(sum(*denom.value)) | bigEndianAmount(sum(*denom.fee_withdrawal))
       | SHA-512( *SHA-512(SHA-512(denom.pub) | bigEndian(32, 0x1) | b) )
       | bigEndian(32, 0x00) | bigEndian(32, 0x00)
msg = bigEndian(32, 40) | bigEndian(32, 1200) /* TALER_SIGNATURE_WALLET_RESERVE_WITHDRAW */
    | SHA-512(commit)
~~~

The wallet derives coins and blinding secrets using `GenerateCoin` from a master secret and an integer index.
This is strictly speaking an implementation detail since the master secret is never revealed to any other party,
and might be chosen to be implemented differently.

// todo: discuss with Florian: use reserve.priv as master_secret?

~~~
GenerateCoin(secret, idx) -> (coin, bks)

Inputs:
    secret  secret to derive coin from
    idx     coin index

Output:
    coin    private-public keypair of the coin
    bks     random blinding key secret of length 32 bytes
~~~

`coin` and `blind_secret` are calculated as follows:

~~~
tmp = HKDF(bigEndian(32, idx), master_secret, "taler-withdrawal-coin-derivation", 64)
(coin.priv, bks) = (tmp[:32], tmp[32:])
coin.pub = EdDSA-GetPub(coin.priv)
~~~

(for RSA, without age-restriction)


# Security Considerations

\[ TBD \]

# IANA Considerations

None.

--- back

# Change log

# Acknowledgments
{:numbered="false"}

\[ TBD \]

This work was supported in part by the German Federal Ministry of
Education and Research (BMBF) within the project Concrete Contracts.
