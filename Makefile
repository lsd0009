all: txt html

draft-guetschow-taler-protocol.xml: draft-guetschow-taler-protocol.md
	kramdown-rfc draft-guetschow-taler-protocol.md > draft-guetschow-taler-protocol.xml

html: draft-guetschow-taler-protocol.xml
	xml2rfc --html draft-guetschow-taler-protocol.xml

txt: draft-guetschow-taler-protocol.xml
	xml2rfc draft-guetschow-taler-protocol.xml

